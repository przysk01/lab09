/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;


import java.util.Date;

public interface Term {

    Date getBegin();

    void setBegin(Date begin);
    
    /**
     * Getting duration in minutes
     * @param duration 
     */
    int getDuration();
    
    /**
     * Setting duration in minutes
     * @param duration 
     */
    void setDuration(int duration);
    
    
    
    Date getEnd();
    
}