package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = pk.labs.Lab9.beans.Term.class;
    public static Class<? extends Consultation> consultationBean = pk.labs.Lab9.beans.Consultation.class;
    public static Class<? extends ConsultationList> consultationListBean = pk.labs.Lab9.beans.ConsultationList.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = pk.labs.Lab9.beans.ConsultationListFactory.class;
    
}
